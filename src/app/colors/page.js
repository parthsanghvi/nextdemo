import React from 'react'
import FavoriteColor from '../components/FavoriteColor'

export default function colors() {
  return (
    <div>
      <FavoriteColor item={'colors'}></FavoriteColor>
    </div>
  )
}
