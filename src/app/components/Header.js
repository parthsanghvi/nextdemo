import React from 'react'
import Navbar from "@/app/components/Navbar"
import Alert from "@/app/components/Alert";

const Header = ({mode,toggleMode}) => {
  return (
    <>
<Navbar title="Demo" mode={mode} toggleMode={toggleMode} aboutText="About" contactText="Contact" />
{/* <Alert alert={props.alert} /> */}
</>
  )
}

export default Header
