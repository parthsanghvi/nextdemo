import React, { useEffect, useState } from 'react'
import { Modal, Button, Form } from "react-bootstrap";

const UserInfoForm = ({ onSubmit }) => {
  const [email, setEmail] = useState("");
  const [businessname, setBusinessname] = useState("");
  return (
    <Form onSubmit={onSubmit}>
      <Form.Group controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          name="email"
          required
          onChange={(e) => setEmail(e.target.value)}
        />

        <Form.Label>Business Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter business name"
          value={businessname}
          name="businessname"
          required
          onChange={(e) => setBusinessname(e.target.value)}
        />
      </Form.Group>
      <button type="submit" className="btn btn-primary my-3">Submit</button>
    </Form>
  );
};

function ModalDialog({ sendBusinessName, showpopup }) {

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  //console.log('proppass '+showpopup);
  //setShow(true);
  useEffect(() => {
    //console.log(showpopup);
    if (showpopup) {
      //console.log('modalopen');
      setShow(true);
    }
  }, [showpopup])


  /*useEffect(() => {
    setShow(true);
  }, [false])*/

  const onUserFormSubmit = (e) => {
    e.preventDefault();
    localStorage.setItem('userEmail', e.currentTarget.email.value);
    localStorage.setItem('userBusinessName', e.currentTarget.businessname.value);
    sendBusinessName(e.currentTarget.businessname.value);
    //setShow(false);
    handleClose();

  }

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Enter Your Email ID</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <UserInfoForm onSubmit={onUserFormSubmit} />
        </Modal.Body>
      </Modal>
    </>
  )
}

export default ModalDialog