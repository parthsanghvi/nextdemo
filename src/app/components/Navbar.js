import React from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import Image from 'next/image';

export default function Navbar({mode,toggleMode,title,aboutText,contactText}) {
  return (
    <nav className={`navbar navbar-expand-lg navbar-${mode} bg-${mode}`}>
      <div className="container-fluid">
        <Link className="navbar-brand" href="/"><Image
      src="/logo.png"
      width={128}
      height={51}
      alt={title}
    /></Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <Link className="nav-link active" aria-current="page" href="/">Home</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" href="/fonts">Fonts</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" href="/colors">Colors</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" href="/about">{aboutText}</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" href="/contact">{contactText}</Link>
            </li>
          </ul>
          <div className={`form-check form-switch text-${mode === 'light' ? 'dark' : 'light'}`}>
            <input className="form-check-input" type="checkbox" onClick={toggleMode} role="switch" id="flexSwitchCheckDefault" />
            <label className="form-check-label" htmlFor="flexSwitchCheckDefault">Enable DardMode</label>
          </div>
        </div>
      </div>
    </nav>
  )
}

Navbar.prototype = {
  title: PropTypes.string.isRequired,
  aboutText: PropTypes.string,
  contactText: PropTypes.string
}