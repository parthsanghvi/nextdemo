import {
    Modern_Antiqua,
    Bebas_Neue,
    Style_Script,
    Roboto_Slab,
    Exo, 
    Lato, 
    Open_Sans, 
    PT_Sans, 
    Roboto,
    Source_Sans_3, 
    Bitter, 
    Lora, 
    Merriweather,
    Noto_Serif,
    Playfair_Display,
    PT_Serif
     } from 'next/font/google'

const modern_antiqua = Modern_Antiqua({ subsets: ['latin'], weight: '400'})
const bebas_neue = Bebas_Neue({ subsets: ['latin'], weight: '400'})
const style_script = Style_Script({ subsets: ['latin'], weight: '400'})
const roboto_slab = Roboto_Slab({ subsets: ['latin'], weight: '400'})
const exo = Exo({ subsets: ['latin'] })
const lato = Lato({ weight: ['400'], subsets: ['latin'] })
const open_sans = Open_Sans({ weight: ['400'], subsets: ['latin'] })
const pt_sans = PT_Sans({ weight: ['400'], subsets: ['latin']})
const roboto = Roboto({ weight: ['400'], subsets: ['latin'] })
const source_sans_3 = Source_Sans_3({ weight: ['400'], subsets: ['latin'] })
const bitter = Bitter({ weight: ['400'], subsets: ['latin'] })
const lora = Lora({ weight: ['400'], subsets: ['latin'] })
const merriweather = Merriweather({ weight: ['400'], subsets: ['latin'] })
const noto_serif = Noto_Serif({ weight: ['400'], subsets: ['latin'] })
const playfair_display = Playfair_Display({ weight: ['400'], subsets: ['latin'] })
const pt_serif = PT_Serif({ weight: ['400'], subsets: ['latin'] })


export const fontMap = {
    modern_antiqua: modern_antiqua.className,
    bebas_neue: bebas_neue.className,
    style_script: style_script.className,
    roboto_slab: roboto_slab.className,
    exo: exo.className,
    lato: lato.className,
    open_sans: open_sans.className,
    pt_sans: pt_sans.className,
    roboto: roboto.className,
    source_sans_3: source_sans_3.className,
    bitter: bitter.className,
    lora: lora.className,
    merriweather: merriweather.className,
    noto_serif: noto_serif.className,
    playfair_display: playfair_display.className,
    pt_serif: pt_serif.className,
}