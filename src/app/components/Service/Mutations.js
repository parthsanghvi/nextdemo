import { gql } from '@apollo/client';

export const LOGIN_USER = gql`
mutation LoginUser(
 $username: String!
 $password: String!
 $clientMutationId: String!
) {
 login(
   input: {
     username: $username
     password: $password
     clientMutationId: $clientMutationId
   }
 ) {
   authToken
   refreshToken
   user {
     userId
     firstName
     lastName
     email
   }
 }
}
 `;

export const CREATE_POST = gql`
 mutation CREATE_POST(
   $clientMutationId: String!
   $title: String!
   $content: String!
   $date: String!

 ) {
   createPost(
     input: {
       clientMutationId: $clientMutationId
       title: $title
       status: PUBLISH
       content: $content
       date: $date
     }
   ) {
    post {
      postId
       title
       date
       content
     }
   }
 }
`;

export const GET_POSTS = gql`
query GET_POSTS( $author: Int){
  posts(where: {author:$author}) {
    nodes {
      postId
      title
      date
      content
    }
  }
}
`;

export const USER_REGISTER = gql` mutation REGISTER_USER(
  $username: String!
  $email: String!
  $firstName: String!
  $lastName: String!
  $password: String!

) {
  registerUser(
    input: {
      username: $username
      email: $email
      firstName: $firstName
      lastName: $lastName
      password: $password
    }
  ) {
    user {
      email
      id
    }
  }
}
`;

export const DELETE_POST  = gql` mutation DELETE_POST(
  $clientMutationId: String!
  $id: ID!
) {
  deletePost(
    input: {
      clientMutationId: $clientMutationId
      id: $id
    }
  ) {
    deletedId
    post  {
      id
    }
  }
}
`;

export const UPDATE_POST = gql`
 mutation UPDATE_POST(
   $clientMutationId: String!
   $title: String!
   $content: String!
   $date: String!
   $id: ID!

 ) {
  updatePost(
     input: {
       clientMutationId: $clientMutationId
       title: $title
       status: PUBLISH
       content: $content
       date: $date
       id: $id
     }
   ) {
    post {
      postId
       title
       date
       content
     }
   }
 }
`;

export const CREATE_LOG = gql`
 mutation CREATE_LOG(
   $clientMutationId: String
   $fav_wp_post_id: Int
   $user_email: String
   $item: String
 ) {
  createLogData(
     input: {
       clientMutationId: $clientMutationId
       fav_wp_post_id: $fav_wp_post_id
       user_email: $user_email
       item: $item
     }
   ) {
    success
   }
 }
`;

// fonts {
//   nodes {
//     title
//     fontId
//     featuredImage {
//       node {
//         sourceUrl
//       }
//     }
//   }
// }

export const GET_ALL_FONTS = gql`
  query GET_ALL_FONTS($parent:ID!) {
    fonts(where: {parent:$parent}) {
        nodes {
            title
            excerpt
            fontId
            featuredImage {
              node {
                sourceUrl
             }
             }
          }
    }
  }
`;

export const GET_ALL_COLORS = gql`
  query GET_ALL_COLORS($parent:ID!) {
    colors(where: {parent:$parent}) {
        nodes {
            title
            excerpt
            colorId
            featuredImage {
              node {
                sourceUrl
             }
             }
          }
    }
  }
`;

//Note:
// query NewQuery {
//   fonts(where: {parent: "0"}) {
//     nodes {
//       fontId
//       parent {
//         node {
//           ... on Font {
//             fontId
//           }
//         }
//       }
//     }
//   }
// }