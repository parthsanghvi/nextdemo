import { GoogleFont } from 'next/font/google';
//import { GoogleFont } from '@next/font/google';

export const loadFont = (fontName) => {
  return GoogleFont({
    families: [fontName],
  });
};