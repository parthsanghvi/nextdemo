"use client";

//import Image from 'next/image'
//import styles from './page.module.css'
// import Fonts from './components/Fonts';
import React, { useEffect, useState } from 'react';
//import Alert from './components/Alert';
import ModalDialog from "@/app/components/ModalDialog"
import { useMutation, useQuery } from '@apollo/client';
import { CREATE_LOG, GET_ALL_FONTS, GET_POSTS, GET_ALL_COLORS } from './Service/Mutations';

export default function FavoriteColor(props) {

  const [parentFontID, setParentFontID] = useState(0);
  const [fonts, setFonts] = useState([]);
  const [updateFonts, setUpdateFonts] = useState([]);
  const [setupCount, setsetupCount] = useState(2);

  //Child
  const [updateFontsParent, setupdateFontsParent] = useState([]);
  const [childfontclass, setcChildFontClass] = useState('');

  //Child
  const [title, setTitle] = useState(`Select your favorite ${props.item}`);
  const [title_sub, setSubTitle] = useState('');

  const [modalpopup, setModalpopup] = useState(false);

  let condition_font = false
  //Colors
  if (props.item == 'fonts') {
    condition_font = true;
  }


  const { loading, error, data } = useQuery(GET_ALL_FONTS, {
    variables: { parent: parentFontID }, skip: !condition_font //Version 1.0.2
  });

  const postsFound = Boolean(data?.fonts.nodes.length);

  useEffect(() => {

    //console.log('postfound test' + postsFound)
    console.log(error)
    if (postsFound) {
      //console.log(data.fonts.nodes);
      //console.log(data.fonts);

      let dbpostdata = data.fonts.nodes.map(post => {
        return {
          'id': post.fontId,
          'title': post.title,
          'featured_image_url': post.featuredImage.node.sourceUrl,
          'favfont': false,
          'favfont_render': false,
          'render': false,
        }
      })

      let fontsArray = dbpostdata.sort(() => Math.random() - Math.random()).slice(0, 2);

      dbpostdata = dbpostdata.map((item, i) => {

        if (fontsArray.find(({ id }) => id === item.id)) {
          return { ...item, ['render']: true };
        } else {
          return item;
        }
      });

      // console.log(fontsArray);
      // console.log(dbpostdata);

      setUpdateFonts(fontsArray);
      setFonts(dbpostdata);
    }

    //console.log('loading ' + loading);

  }, [loading]);


  let condition_color = false
  //Colors
  if (props.item == 'colors') {
    condition_color = true;
  }

  const { loading: loadingcolor, error: errorcolor, data: datacolor } = useQuery(GET_ALL_COLORS, {
    variables: { parent: parentFontID }, skip: !condition_color, //Version 1.0.2
  });
  console.log(datacolor);

  const postsFoundColor = Boolean(datacolor?.colors.nodes.length);

  useEffect(() => {

    console.log('postsFoundColor ' + postsFoundColor)
    console.log(errorcolor)
    if (postsFoundColor) {
      //  console.log(data.colors.nodes);
      // console.log(data.colors);
      // console.log('find');
      let dbpostdata = datacolor.colors.nodes.map(post => {
        return {
          'id': post.colorId,
          'title': post.title,
          'featured_image_url': post.featuredImage.node.sourceUrl,
          'favfont': false,
          'favfont_render': false,
          'render': false,
        }
      })

      let fontsArray = dbpostdata.sort(() => Math.random() - Math.random()).slice(0, 2);

      dbpostdata = dbpostdata.map((item, i) => {

        if (fontsArray.find(({ id }) => id === item.id)) {
          return { ...item, ['render']: true };
        } else {
          return item;
        }
      });

      //console.log(fontsArray);
      //console.log(dbpostdata);

      setUpdateFonts(fontsArray);
      setFonts(dbpostdata);
    }

    // console.log('loading ' + loading);

  }, [loadingcolor]);


  const [createLog, { loadingCreateExp, errorCreateExp, dataCreateExp }] = useMutation(CREATE_LOG, {
    onCompleted: (dataCreateExp) => {
      //console.log(dataCreateExp);
      //console.log('sucess' + dataCreateExp);

    }, onError(errorCreateExp) {
      //console.log('error' + errorCreateExp);
      //setError(errorCreateExp.message);
    }
  });

  //function run onclick of fonts select
  const selectedFav = (event) => {

    //check user have enter the email id or not
    setModalpopup(false);
    let userEmail = localStorage.getItem("userEmail");
    //console.log(userEmail);
    //setModalpopup(false);
    if (!userEmail) {
      setTimeout(() => setModalpopup(true), 10);
      //event.preventDefault();
      return false;
    }
    //

    //console.log(fonts);
    //console.log('before');
    setsetupCount(setupCount + 1)

    //let favFontCount = 0;
    let itemID = event.target.dataset.fontid;
    let updatedPost = fonts.map((item, i) => {

      if (item.id == itemID) {
        console.log('Your Favorite font is ' + item.title);
        return { ...item, ['favfont']: true };
      } else {
        return item;
      }
    });

    //console.log('setupCount '+setupCount);
    //console.log(fonts);

    setFonts(updatedPost)
    //console.log('update');
    //console.log(updatedPost);

    if (setupCount == 2) {
      //show 2 random font which not used before
      //console.log(fonts);
      console.log('setsetupCount ' + setupCount);

      let setup2Updated = updatedPost.filter(function (el) {
        return el.render != true;
      });

      setup2Updated = setup2Updated.sort(() => Math.random() - Math.random()).slice(0, 2)

      let setup2font = updatedPost.map((item, i) => {

        if (setup2Updated.find(({ id }) => id === item.id)) {
          return { ...item, ['render']: true };
        } else {
          return item;
        }
      });

      console.log(setup2Updated);
      setUpdateFonts(setup2Updated);
      setFonts(setup2font);
      console.log(setup2font);
    }

    if (setupCount == 3) {
      console.log('setsetupCount ' + setupCount);
      //show 1 fav font + 1 not used before
      console.log(updatedPost);

      let setup3Updated_fav = updatedPost.filter(function (el) {
        return el.favfont == true;
      });

      let setup3Updated_not_render = updatedPost.filter(function (el) {
        return el.favfont != true && el.render != true;
      });

      console.log('third setup fav');
      console.log(setup3Updated_fav);
      console.log(setup3Updated_not_render);

      setup3Updated_fav = setup3Updated_fav.sort(() => Math.random() - Math.random()).slice(0, 1)
      setup3Updated_not_render = setup3Updated_not_render.sort(() => Math.random() - Math.random()).slice(0, 1)

      //setup3Updated_fav = {...setup3Updated_fav, favfont_render: true}
      //console.log(setup3Updated_fav);

      let setup3Updated = [...setup3Updated_fav, ...setup3Updated_not_render];

      let setup3font = updatedPost.map((item, i) => {

        if (setup3Updated_not_render.find(({ id }) => id === item.id)) {
          return { ...item, ['render']: true };
        } else {
          return item;
        }
      });
      console.log('third setup fav add');
      console.log(setup3Updated);
      //setup3Updated
      //setUpdateFonts(setup3Updated);
      setFonts(setup3font);
      setUpdateFonts(setup3Updated);

      let setup3font_fav = setup3font.map((item, i) => {

        if (setup3Updated_fav.find(({ id }) => id === item.id)) {
          return { ...item, ['favfont_render']: true };
        } else {
          return item;
        }
      });
      console.log(setup3font_fav);
      setFonts(setup3font_fav);

    }

    if (setupCount == 4) {
      console.log('setsetupCount ' + setupCount);

      let setup4Updated_fav = updatedPost.filter(function (el) {
        return el.favfont == true && el.favfont_render != true;
      });
      //console.log(setup4Updated_fav);
      setup4Updated_fav = setup4Updated_fav.sort(() => Math.random() - Math.random()).slice(0, 1)

      let setup4Updated_not_render = updatedPost.filter(function (el) {
        return el.favfont != true && el.render != true;
      });

      let setup4Updated = [...setup4Updated_fav, ...setup4Updated_not_render];
      console.log(setup4Updated_fav);
      console.log(setup4Updated_not_render);
      console.log(updatedPost);

      setUpdateFonts(setup4Updated);

      //Show other fav font + 1 not used before
    }

    if (setupCount == 5) {
      //show 2 fav font
      console.log('setsetupCount ' + setupCount);
      console.log(updatedPost);
      let setup5Updated_fav = updatedPost.filter(function (el) {
        return el.favfont == true;
      });
      let setup5Updated = setup5Updated_fav.sort(() => Math.random() - Math.random()).slice(0, 2)
      setUpdateFonts(setup5Updated);
    }

    if (setupCount == 6) {

      console.log('setsetupCount ' + setupCount);
      console.log(updatedPost);
      let setup6Updated = updatedPost.filter(function (el) {
        return el.id == itemID;
      });
      //setUpdateFonts('');
      //setUpdateFonts(updatedPost);

      setTitle(`Your parent selected favorite ${props.item} is`);
      console.log('Your selected favorite font is ' + itemID);
      setSubTitle(`Now please select child ${props.item} of parent`);
      console.log(setup6Updated);
      setParentFontID(itemID);

      if (parentFontID == 0) {
        setupdateFontsParent(setup6Updated);
        setsetupCount(2);
        setUpdateFonts('');
        setcChildFontClass('child_fonts');
      } else {
        setSubTitle(`Your child selected favorite ${props.item} is`);
        setUpdateFonts(setup6Updated)
      }
      //Show final result
      //setupdateFontsParent(setup6Updated);
      //setsetupCount(2);
      console.log('parentFontID ' + parentFontID);

      createLog({
        variables: {
          clientMutationId: "3XwOkgW324g4545",
          fav_wp_post_id: parseInt(itemID),
          user_email: localStorage.getItem("userEmail"),
          item: props.item
        }
      });
    }

  };


  return (
    <>
      <div className='container font-section my-5'>
        <h3 className="text-center my-3">{title}</h3>
        {updateFontsParent && updateFontsParent.length > 0 &&
          <div className='row parent_fonts'>
            {updateFontsParent && updateFontsParent.length > 0 && updateFontsParent.map((item, index) => (
              <div key={item.id} className="col-sm text-center">
                <img data-fontid={item.id} src={item.featured_image_url} className="App-logo font-logo" alt={item.title} />
                <h5 className={`text-center style_script my-3 `} >{item.title}</h5>
              </div>
            ))}
          </div>
        }

        <h4 className="text-center mt-5 mb-3">{title_sub}</h4>
        {updateFonts && updateFonts.length > 0 &&
          <div className={`row ${childfontclass}`}>
            {updateFonts && updateFonts.length > 0 && updateFonts.map((item, index) => (
              <div key={item.id} className="col-sm text-center">
                <img data-fontid={item.id} src={item.featured_image_url} onClick={selectedFav} className="App-logo font-logo" alt={item.title} />
                <h5 className={`text-center style_script my-3`} >{item.title}</h5>
              </div>
            ))}
          </div>
        }

        <ModalDialog showpopup={modalpopup} />
      </div>
    </>
  );
}