import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const httpLink = createHttpLink({
  //uri: 'http://localhost/reactdemo_wp/graphql',
uri: 'https://dev-expenseadmin.pantheonsite.io/graphql',

  
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const user = JSON.parse(localStorage.getItem('expense_user'));

  return {
    headers: {
      ...headers,
      authorization: user ? `Bearer ${user.login.authToken}` : "",
    }
  }
});

 const link = createHttpLink({
  //uri: "http://localhost/reactdemo_wp/graphql"
  uri: "https://dev-expenseadmin.pantheonsite.io/graphql"

  
});

  const client = new ApolloClient({
   // link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
    link
  });

export default client;